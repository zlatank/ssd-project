# Secure software development and web security - Exam project

## Authors

* Marc Jabbour (jabbour.marc@ulb.be)
* Kovacevic Zlatan (zlatan.kovacevic@ulb.be)

## Date

* 15/08/2022

## User Manual


### ** Prerequisites **

#### Software
Before you can run the program you need to install mySQL and create a root account in order to access the features of the software and our application.

Here is a link to download mySQL: https://dev.mysql.com/downloads/windows/installer/8.0.html


#### Packages
The code uses multiple external libraries listed bellow.

If you are under Windows, launch the command prompt, if you are under Linux, launch the terminal and use the following commands:

> pip install mysql-connector

> pip install pycryptodome

> pip install pyopenssl

> pip install pyjwt

> pip install pyjwt[crypto]

### Instructions

Load the database
Open the terminal and enter the following commands:

Then you have to start the server :
> python3 server.py

Once the server is running you can run the client application using :
> python3 client.py







