from OpenSSL import crypto
import datetime
import key_setup
from cryptography import x509

CA_CERT_FILE = "ca.crt"
CERT_FILE = "servercert.crt"

# CA Certificate key import

a = key_setup.import_private('private.pem')
b = key_setup.get_public_key(a)
c = key_setup.mem_to_pem(b)
d = key_setup.mem_to_pem(a)

pukey = crypto.load_publickey(crypto.FILETYPE_PEM, c)
pkey = crypto.load_privatekey(crypto.FILETYPE_PEM, d)

# Server Certificate key generation

a1 = key_setup.import_private('private(server).pem')
b1 = key_setup.get_public_key(a1)
c1 = key_setup.mem_to_pem(b1)
d1 = key_setup.mem_to_pem(a1)

pukey1 = crypto.load_publickey(crypto.FILETYPE_PEM, c1)

ca_extensions = [
    crypto.X509Extension(b'basicConstraints', True, b'CA:TRUE'),
    crypto.X509Extension(b'keyUsage', False, b'digitalSignature, nonRepudiation'),
    crypto.X509Extension(b'extendedKeyUsage', True, b'serverAuth'),
    crypto.X509Extension(b'subjectAltName', False, b'IP:127.0.0.1')
    ]

extensions = [
    crypto.X509Extension(b'basicConstraints', True, b'CA:FALSE'),
    crypto.X509Extension(b'subjectAltName', False, b'IP:127.0.0.1')
    ]

def create_root_ca_cert():

    # create a self-signed cert
    cert = crypto.X509()
    cert.get_subject().C = "BE"
    cert.get_subject().ST = "Brussels"
    cert.get_subject().L = "Ixelles"
    cert.get_subject().O = "ULB"
    cert.get_subject().OU = "SSD Project"
    cert.get_subject().CN = "172.0.0.1"

    cert.set_serial_number(x509.random_serial_number())
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(10*365*24*60*60)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(pukey)
    cert.add_extensions(ca_extensions)
    cert.sign(pkey, 'sha256')

    print(cert)

    open(CA_CERT_FILE, "wt").write(
        crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode())

    return cert

def create_self_signed_cert():

    # create a self-signed cert
    cert1 = crypto.X509Req()
    cert1.get_subject().C = "BE"
    cert1.get_subject().ST = "Brussels"
    cert1.get_subject().L = "Ixelles"
    cert1.get_subject().O = "ULB"
    cert1.get_subject().OU = "SSD Project"
    cert1.get_subject().CN = "172.0.0.1"

    cert1.set_pubkey(pukey1)
    cert1.sign(pkey, 'sha256')

    #cert1.set_serial_number(x509.random_serial_number())
    cert2 = crypto.X509()
    cert2.gmtime_adj_notBefore(0)
    cert2.gmtime_adj_notAfter(10*365*24*60*60)
    cert2.set_issuer(cert1.get_subject())
    cert2.set_subject(cert1.get_subject())
    cert2.set_pubkey(pukey1)
    cert2.add_extensions(extensions)
    cert2.sign(pkey, 'sha256')

    print(cert2)

    open(CERT_FILE, "wt").write(
        crypto.dump_certificate(crypto.FILETYPE_PEM, cert2).decode())

root_ca = create_root_ca_cert()
create_self_signed_cert(root_ca)


