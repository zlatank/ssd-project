import json, ssl
from re import A
import socket
import os.path
from pathlib import Path
from cryptography.fernet import Fernet

addr = ("127.0.0.2", 5000)
purpose = ssl.Purpose.SERVER_AUTH
context = ssl.create_default_context(purpose, cafile="servercert.crt")
context.verify_mode = ssl.CERT_REQUIRED
#context.load_verify_locations("ca.crt")
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(addr)
ssl_client = context.wrap_socket(client,server_hostname="127.0.0.2")

# Check Certificate Authenticity
cert = ssl_client.getpeercert()
ssl.match_hostname(cert, "127.0.0.2")

check = True
logged_in = False
# key = Fernet.generate_key()

def generate_master_key():
    
    #get client directory
    directory_path = os.getcwd()
    my_key_file = directory_path
    
    #check if key exists
    if os.path.exists(my_key_file):
        with open(my_key_file, 'rb') as myfile:
            master_key = myfile.read()
            
    else:
        master_key = Fernet.generate_key()
        with open(my_key_file, 'wb') as myfile:
            myfile.write(master_key)
    return master_key

def encrypt(key, fin, fout, *, block = 1 << 16):
    import cryptography.fernet, struct
    fernet = cryptography.fernet.Fernet(key)
    with open(fin, 'rb') as fi, open(fout, 'wb') as fo:
        while True:
            chunk = fi.read(block)
            if len(chunk) == 0:
                break
            enc = fernet.encrypt(chunk)
            fo.write(struct.pack('<I', len(enc)))
            fo.write(enc)
            if len(chunk) < block:
                break

def decrypt(key, fin, fout):
    import cryptography.fernet, struct
    fernet = cryptography.fernet.Fernet(key)
    with open(fin, 'rb') as fi, open(fout, 'wb') as fo:
        while True:
            size_data = fi.read(4)
            if len(size_data) == 0:
                break
            chunk = fi.read(struct.unpack('<I', size_data)[0])
            dec = fernet.decrypt(chunk)
            fo.write(dec)

def enc_file1(word,key):
    import cryptography.fernet
    
    f = cryptography.fernet.Fernet(key)
    
    word = bytes(word, 'utf-8') 
    encyrpted = f.encrypt(word)
    return encyrpted

def dec_file1(word,key):
    import cryptography.fernet
    
    f = cryptography.fernet.Fernet(key)
    
    word = bytes(word, 'utf-8') 
    encyrpted = f.decrypt(word)
    return encyrpted
  
def send_file():
    import cryptography.fernet, secrets
    key = generate_master_key()

    print("Enter the location of the file to send:")
    path = input()
    check_path = Path(path)
    if check_path.is_file():    
        FILENAME = check_path
        # FILESIZE = os.path.getsize(FILENAME)
        file_name = os.path.basename(FILENAME)
        FILENAME= os.path.splitext(file_name)[0]
        FILENAME =enc_file1(FILENAME,key) 
        FILENAME= FILENAME.decode("utf8")
        print((FILENAME))

        data = f"{FILENAME}"
        ssl_client.send(data.encode("utf8"))
        msg = ssl_client.recv(1024).decode("utf8")
        print(f"SERVER: {msg}")
        encrypt(key, path, 'data10101010.AXX')
        # decrypt(key, 'data10101010.AXX', 'data12121212.txt')
        with open('data10101010.AXX','r') as f:
            while True:
                data = f.read(1024).encode('utf8') 
                if not data:
                    print("File sent to the server")
                    break

                ssl_client.send(data)

    else:
        print("The file  does not exist! Please enter a valid path of the file to send")
        send_file()
    
    # run()

def ask():
    print("Please choose what to do:")
    print("1.Send a file")
    print("2.Receive a file")
    ch = int(input("Enter your choice: "))

    return ch 
 
def receiving_file():
    key = generate_master_key()   
    status = True
    print("Available files:")
    while status:

        data = ssl_client.recv(1024).decode("utf8")

        if  data =="done":       
            status=False 
            break 
        # data= dec_file1(data,key)
        print(data)\
  
    print("Choose the file name to receive:")
    chosen_file = input()
    ssl_client.send(chosen_file.encode("utf8"))
    status=True
    with open( chosen_file +".txt", 'w') as f:
        while status:

            data = ssl_client.recv(1024).decode("utf8")
            data = dec_file1(data,key)
            # print(data)

            if data =="done":
                status=False
                print("File Received")
                break

            f.write(data)
    
def run():
    ch = ask()
    if ch == 1:
        msg="upload_request"
        ssl_client.send(msg.encode('utf8'))
        send_file()
    if ch == 2:
        msg="read_request"
        ssl_client.send(msg.encode('utf8'))
        receiving_file()
    

