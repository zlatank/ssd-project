from Crypto.PublicKey import RSA
import os


def export_key(private_key):
    #print("Paste the path to save the key:") 
    #path = input() 
    directory_path = os.getcwd()
    full_path = directory_path + "\private(server).pem"
    with open(full_path, 'wb') as f:
        f.write(private_key.exportKey('PEM'))
        f.close()

def import_private(key_name):
    with open(key_name, "r") as f:
            private_key = RSA.importKey(f.read())
    return private_key

def mem_to_pem(key):
    pem_key = key.exportKey().decode()
    return pem_key

def get_keypair():
    private_key = RSA.generate(2048)
    return private_key

def get_public_key(private_key):
    public_key = private_key.publickey()
    return public_key

#a = import_private('private.pem')
#b = get_public_key(a)
#print(mem_to_pem(b))

### Generating Server Keypair ###

#key = get_keypair()
#print("Keypair generated")
#export_key(key)
#print("Keypair exported")
#import_private('private.pem')
