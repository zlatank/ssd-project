
import socket, ssl
import threading, json
import os, requests
from dbmanagement import active_token_request, remove_request
from jwt_auth import get_token

def listener():

    host = "127.0.0.1"
    port = 5000
    addr = (host,port)
    cert = "./servercert.crt"
    ca_cert = "./ca.crt"
    server_key = "./private(server).pem"
    
    
    context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH,cafile=cert)
    context.load_cert_chain(certfile=cert, keyfile=server_key)

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))

    print(host)

    server_socket.listen()
    print(f"[LISTENING] Server is listening on {host}." )
    while True:
        conn, addr = server_socket.accept()
        conn = context.wrap_socket(conn, server_side=True)
        threading.Thread(target=clientf, args=(conn, addr)).start()
        print(f"[ACTIVE CONNECTION] {threading.activeCount()-1} connected.")

def clientf(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")

    while True:
        data = conn.recv(64).decode()

        if not data :
            break

        print(data)
    
        str = "Message received"
        str = str.encode()
        conn.send(str)

        if data == "stop":

            conn.close()
            print('client disconenct')
            break

        if data == "TOKEN_REQ":

            send_token(conn)
            #CONNECT TO FTP
            conn.close()

            break

def send_token(conn):

    try:
        str = "Request received"
        str = str.encode()
        conn.send(str)
        username = conn.recv(5).decode()
        str = "Username received"
        str = str.encode()
        conn.send(str)
        
        if active_token_request(username):

            token = get_token(username)
            wrapped_token = json.dumps(token)
            conn.sendall(wrapped_token)
            remove_request(username)
            print("Token sent!")

    except: print("ERROR: Token request processiong failed")

listener()



