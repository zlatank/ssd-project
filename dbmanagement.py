from asyncio.windows_events import NULL
from dataclasses import field
import re
import mysql.connector
import bcrypt


db = mysql.connector.connect(
host="localhost",
user="root",
password="admin",
database="user_data"
)

cursor = db.cursor(buffered=True)

def makedb():
    cursor.execute("CREATE DATABASE user_data")

def listdb():
    cursor.execute("SHOW DATABASES")

    for x in cursor:
        print(x)

def addtable():
    cursor.execute("CREATE TABLE userinfo (username VARCHAR(255), salt VARCHAR(255),password VARCHAR(255), token_request INT)")

def addtable1():
    cursor.execute("CREATE TABLE fileinfo (username VARCHAR(255), file_ID INT AUTO_INCREMENT,enc_fname VARCHAR(255))")

def listtable():
    cursor.execute("SHOW TABLES")

    for x in cursor:
        print(x)

def addline(val):

    sql = "INSERT INTO userinfo (username, salt, password, token_request) VALUES (%s, %s, %s, 0)"
    #val = ("root", "salt", "secret", "admin")
    cursor.execute(sql, val)

    db.commit()

def addfile(val):
    sql = "INSERT INTO fileinfo (username, enc_fname) VALUES (%s, %s)"

    cursor.execute(sql, val)

    db.commit()


def showfile(username):
  
    cursor.execute('SELECT enc_fname FROM fileinfo WHERE username = %s',(username,))

    enc_fname= cursor.fetchall()
    return enc_fname

def selectfile(filename):
  
    cursor.execute('SELECT file_ID FROM fileinfo WHERE enc_fname = %s',(filename,))

    file_ID= cursor.fetchone()
    return file_ID
    
def file_nb(filename):

    cursor.execute('SELECT file_ID FROM fileinfo WHERE enc_fname = %s',(filename,))

    file_number = cursor.fetchone()
    return file_number
    
def active_token_request(user):
    cursor.execute('SELECT active_token FROM userinfo WHERE username = %s', user)
    request = cursor.fetchone()
    if request == 1:
        return True

    else: 
        return False

def remove_request(username):
    cursor.execute('UPDATE userinfo SET token_request = %d WHERE username = %s', 1, username)

def add_token_column():
    query= "ALTER TABLE userinfo ADD COLUMN token_request BOOL AFTER password"
    execute(query)

def showtable():
    query = "SHOW TABLES"
    cursor.execute(query)
    for x in cursor:
        print(x)

def execute(query, val):
    cursor.execute(query, val)

def fetchall():
    return (cursor.fetchall())

def fetchone():
    return (cursor.fetchone())

def closeconn():
    db.close()
def askUser():
    print("********** Welcome **********")
    print("1.Signup")
    print("2.Login")
    ch = int(input("Enter your choice: "))
    if ch == 1:
        askRegister()
    elif ch == 2:
        askLogin()
    else:
        print("Wrong Choice!")
        print ("Please choose one of the available options")
        askUser()
            
def askRegister():
    print("********** Register System **********")
    username = input("Enter your username: ")
    password = input("Enter your password: ")
    conf_pwd = input("Confirm password: ")
    if conf_pwd == password:
        cursor.execute('SELECT * FROM userinfo WHERE username = %s', (username,))
        account = cursor.fetchone()
        # If account exists show error and validation checks
        if account:
            print ("Account already exists!")
            print("login or create an account with other username")
            askUser()
        elif not re.match(r'[A-Za-z0-9]+', username):
            print ("Username must contain only characters and numbers and also cant be kept empty!")
            askRegister()
        # elif not re.match(r'[A-Za-z]{8,}', password):
        #     print ("password must have at least 8 characters")
        #     askRegister()            
        elif not username or not password:
            print ("Please fill in a username or password!")
            askRegister()
        else:
            # Account doesnt exists, now insert new account into db table
            register(username,password)

    else:
        print("Passwords does not match!")
        print("Please Retry!")
        askRegister()

        
def register(username,password):

        salt=bcrypt.gensalt()
        # print(salt)
        hashed = bcrypt.hashpw(password.encode('utf8'), salt)
        cursor.execute('INSERT INTO userinfo (username,salt,password,token_request) VALUES (%s,%s, %s,%d)', (username,salt,hashed,0))
        db.commit()
        print ("You have successfully registered!")
        askUser()


def askLogin():
    print("********** Login System **********")
    username = input("Enter your username: ")
    password = input("Enter your password: ")
    login_attempt = checkPass(username, password)
    if login_attempt == True:
        return username



def checkPass(user, pwd):   
    cursor.execute('SELECT * FROM userinfo WHERE username = %s', (user,))
    account = cursor.fetchone()
    # print(account)
    if account:

         check = login(user,pwd)
         if check != NULL:
            return True
    else:
        print ("Your username is incorrect!")
        askLogin()

def login(user,pwd):
    cursor.execute('SELECT password FROM userinfo WHERE username = %s', (user,))
    hashed = cursor.fetchone()
    # print(hashed[0])
    hashed = hashed[0].encode('utf8')
    pwd = pwd.encode('utf8')
    if bcrypt.checkpw(pwd, hashed):

        print ("You have successfully logged in!")
        print ("********** Welcome " + user + " ********** ") 
        cursor.execute('INSERT INTO userinfo (token_request) VALUE %s', 1)

        askUser()
        return user
    else:
        print("Incorrect password! Please try again")
        askUser()

