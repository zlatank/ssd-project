from cgi import test
import socket
import ssl,json
from asyncio.windows_events import NULL
from dbmanagement import askLogin
from FTP_Server import ftp_clientf

# 

addr = ("127.0.0.1", 5000)
purpose = ssl.Purpose.SERVER_AUTH
context = ssl.create_default_context(purpose, cafile="servercert.crt")
context.verify_mode = ssl.CERT_REQUIRED
#context.load_verify_locations("ca.crt")
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(addr)
ssl_client = context.wrap_socket(client,server_hostname="127.0.0.1")

# Check Certificate Authenticity
cert = ssl_client.getpeercert()
ssl.match_hostname(cert, "127.0.0.1")

#cert = client.getpeercert()
check = True
logged_in = False

def take_input():
    string = input()

    return string

def transmit():
    message = take_input()
    ssl_client.send(message.encode())

    from_server = ssl_client.recv(64).decode()
    print(from_server)

    if message == "stop":
        ssl_client.close()
        print("disconnected")
    
    return message

def request_token(username):
    ssl_client.send("TOKEN_REQ")
    from_server = ssl_client.recv(64).decode()
    ssl_client.send(username)
    from_server = ssl_client.recv(64).decode()

    if from_server == "Username received":
        from_server = ssl_client.recv(1024).decode()
        token = json.loads(from_server)
        print("Token sent")
        return token

print("Connected!")
#askLogin()
check = transmit()

while check != "stop":
    
    if not logged_in:
        username = askLogin()

    else:
       token = request_token(username)

       if token != NULL:
        
        ssl_client.close()
        import test_client
        test_client.run()

    check = transmit()





        